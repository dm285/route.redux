const logIn = (authObject) => {
    return {
        type: "LOG_IN",
        payload: authObject
    }
}

const logOut = () => {
    return {
        type: "LOG_OUT",
        payload: { token: 'empty', status: 'Logged out' }
    }
}

export const getTicket = (authObject) => authObject.token;

export default {
    logIn,
    logOut
}


