import './css/Component.css';
import Header from './Header';

export default function NotFoundPage() {
    return (
        <div>
            <Header Title={'Not found page'} />
        </div>
    );
}