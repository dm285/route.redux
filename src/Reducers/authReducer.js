const initialState = {
    token: ''
}

const AuthReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'LOG_IN':
            return {
                token: action.payload.token,
                status: action.payload.status
            }

        case 'LOG_OUT':
            return {
                token: '',
                status: action.payload.status
            }

        default:
            return state
    }
}

export default AuthReducer